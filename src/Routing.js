import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Splashscreen from './screen/Splashscreen';
import AuthNavigation from './AuthNavigation';
import HomeScreen from './screen/HomeScreen';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import TransactionScreen from './screen/TransactionScreen';
import ProfileScreen from './screen/ProfileScreen';
import DetailScreen from './screen/DetailScreen';
import FormPemesanan from './screen/FormPemesanan';
import Keranjang1 from './screen/Keranjang1';
import Summary from './screen/Summary';
import ReservasiSukses from './screen/ReservasiSukses';
import MyTabBar from './MytabBar';
import KodeReservasi from './screen/KodeReservasi';
import CheckOutScreen from './screen/CheckOutScreen';
import EditProfile from './screen/EditProfile';
import FAQScreen from './screen/FAQScreen';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();
function MainRoute() {
  return (
    <Tab.Navigator
      tabBar={props => <MyTabBar {...props} />}
      screenOptions={{headerShown: false}}
      initialRouteName="Home">
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="Transaction" component={TransactionScreen} />
      <Tab.Screen name="Profile" component={ProfileScreen} />
    </Tab.Navigator>
  );
}
const Routing = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="SplashScreen" component={Splashscreen} />
        <Stack.Screen name="AuthNavigation" component={AuthNavigation} />
        <Stack.Screen name="HomeScreen" component={HomeScreen} />
        <Stack.Screen name="MainRoute" component={MainRoute} />
        <Stack.Screen name="DetailScreen" component={DetailScreen} />
        <Stack.Screen name="FormPemesanan" component={FormPemesanan} />
        <Stack.Screen name="Keranjang1" component={Keranjang1} />
        <Stack.Screen name="Summary" component={Summary} />
        <Stack.Screen name="ReservasiSukses" component={ReservasiSukses} />
        <Stack.Screen name="KodeReservasi" component={KodeReservasi} />
        <Stack.Screen name="CheckOutScreen" component={CheckOutScreen} />
        <Stack.Screen name="EditProfile" component={EditProfile} />
        <Stack.Screen name="FAQScreen" component={FAQScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default Routing;
