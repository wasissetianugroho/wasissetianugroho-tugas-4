import React from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';

function MyTabBar({state, descriptors, navigation}) {
  return (
    <View style={{flexDirection: 'row'}}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({name: route.name, merge: true});
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{
              flex: 1,
              backgroundColor: 'white',
              alignItems: 'center',
              paddingTop: 20,
              paddingBottom: 16,
            }}>
            <Image
              style={{
                resizeMode: 'contain',
                width: 20,
                height: 20,
                tintColor: isFocused ? '#BB2427' : '#D8D8D8',
              }}
              source={checkLogo(label)}
            />
            <Text
              style={{
                color: isFocused ? '#BB2427' : '#D8D8D8',
                marginTop: 3,
                fontSize: 10,
                fontWeight: 'bold',
              }}>
              {label}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}
const checkLogo = label => {
  if (label == 'Home') {
    return require('./assets/icon/Home.png');
  } else if (label == 'Profile') {
    return require('./assets/icon/Profile.png');
  } else if (label == 'Transaction') {
    return require('./assets/icon/Transaction.png');
  }
};

export default MyTabBar;
