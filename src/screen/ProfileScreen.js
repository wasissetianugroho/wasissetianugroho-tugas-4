import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Routing from '../Routing';
const ProfileScreen = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <ScrollView>
        {/* Profile ========================================================================================= */}
        <View style={{backgroundColor: 'white', padding: 10}}>
          <Image
            style={{
              width: 95,
              height: 95,
              borderRadius: 10,
              marginTop: 55,
              alignSelf: 'center',
            }}
            source={require('../assets/images/Wasis.jpg')}
          />
          <Text style={{fontSize: 20, alignSelf: 'center', color: 'black'}}>
            Wasis Setia Nugroho
          </Text>
          <View>
            <Text
              style={{
                color: '#A8A8A8',
                fontSize: 10,
                alignSelf: 'center',
              }}>
              wasissetianugroho@gmail.com
            </Text>
            <TouchableOpacity
              onPress={() => navigation.navigate('EditProfile')}>
              <Text
                style={{
                  color: '#050152',
                  backgroundColor: '#F6F8FF',
                  borderRadius: 15,
                  fontSize: 15,
                  fontWeight: '700',
                  alignSelf: 'center',
                  padding: 5,
                  marginTop: 35,
                  marginBottom: 20,
                  paddingHorizontal: 20,
                }}>
                Edit
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View>
          <View
            style={{
              flex: 1,
              padding: 20,
              backgroundColor: 'white',
              marginTop: 10,
              borderRadius: 8,
              marginEnd: 10,
              marginStart: 10,
            }}>
            <View style={{flexDirection: 'row', width: '80%'}}>
              <View style={{marginStart: 10}}>
                <View
                  style={{
                    flexDirection: 'column',
                    alignItems: 'baseline',
                  }}>
                  <View>
                    <Text
                      style={{
                        paddingTop: 10,
                        color: 'black',
                        fontSize: 19,
                        fontWeight: '900',
                        paddingHorizontal: 50,
                      }}>
                      About
                    </Text>
                  </View>
                  <Text
                    style={{
                      paddingTop: 10,
                      color: 'black',
                      fontSize: 19,
                      fontWeight: '900',
                      paddingHorizontal: 50,
                    }}>
                    Terms & Condition
                  </Text>
                  <TouchableOpacity
                    onPress={() => navigation.navigate('FAQScreen')}>
                    <Text
                      style={{
                        paddingTop: 10,
                        color: 'black',
                        fontSize: 19,
                        fontWeight: '900',
                        paddingHorizontal: 50,
                      }}>
                      FAQ
                    </Text>
                  </TouchableOpacity>
                  <Text
                    style={{
                      paddingTop: 10,
                      color: 'black',
                      fontSize: 19,
                      fontWeight: '900',
                      paddingHorizontal: 50,
                    }}>
                    History
                  </Text>
                  <Text
                    style={{
                      paddingTop: 10,
                      color: 'black',
                      fontSize: 19,
                      fontWeight: '900',
                      paddingHorizontal: 50,
                    }}>
                    Setting
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>
        {/* Tombol Log Out ===================================================================================== */}
        <View
          style={{
            padding: 15,
            backgroundColor: 'white',
            borderRadius: 8,
            marginEnd: 10,
            marginStart: 10,
            paddingTop: 10,
            marginTop: 10,
            alignItems: 'center',
          }}>
          <TouchableOpacity>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{
                  width: 25,
                  height: 25,
                  color: '#BB2427',
                }}
                source={require('../assets/icon/log_out.png')}
              />
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: '600',
                  color: '#EA3D3D',
                }}>
                Log Out
              </Text>
              <View></View>
            </View>
          </TouchableOpacity>
          <View></View>
        </View>
      </ScrollView>
    </View>
  );
};
export default ProfileScreen;
