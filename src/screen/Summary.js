import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Routing from '../Routing';
const Summary = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <ScrollView>
        {/* Form Pesan ====================================================================================== */}
        <View
          style={{
            flexDirection: 'row',
            padding: 25,
            backgroundColor: 'white',
            bottom: 10,
          }}>
          <TouchableOpacity onPress={() => navigation.navigate('Keranjang1')}>
            <Image
              source={require('../assets/icon/Back.png')}
              style={{
                width: 35,
                height: 25,
                marginTop: 5,
                marginEnd: 5,
              }}
            />
          </TouchableOpacity>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'flex-start',
              paddingStart: 7,
            }}>
            <Text
              style={{
                fontSize: 28,
                fontWeight: '700',
                color: 'black',
              }}>
              Summary
            </Text>
          </View>
        </View>
        {/* Data Cust ========================================================================================= */}
        <View style={{padding: 10, backgroundColor: 'white'}}>
          <View>
            <Text
              style={{
                paddingBottom: 10,
                fontSize: 20,
                color: '#979797',
              }}>
              Data Customer
            </Text>
            <Text style={{fontSize: 20}}>Wasis Setia N ( 081393530013 )</Text>
            <Text style={{fontSize: 20}}>
              Jl.perumnas, Condong catur, Sleman, Yogyakarta
            </Text>
            <Text style={{fontSize: 20}}>gantengdoang@dipanggang.com </Text>
          </View>
        </View>
        {/* Alamat Tujuan ===================================================================================== */}
        <View style={{padding: 8, backgroundColor: 'white', marginTop: 10}}>
          <View>
            <Text
              style={{
                paddingBottom: 10,
                fontSize: 20,
                color: '#979797',
              }}>
              Alamat Outlet Tujuan
            </Text>
            <Text style={{fontSize: 20}}>
              Jack Repair - Seturan (027-343457)
            </Text>
            <Text style={{fontSize: 20}}>
              Jl. Affandi No 18, Sleman, Yogyakarta
            </Text>
          </View>
        </View>
        {/* Sepatu Pilihan ==================================================================================== */}
        <View>
          <View
            style={{
              flex: 1,
              padding: 20,
              backgroundColor: 'white',
              marginTop: 10,
            }}>
            <View style={{flexDirection: 'row', width: '80%'}}>
              <TouchableOpacity>
                <Text
                  style={{
                    paddingBottom: 10,
                    fontSize: 14,
                  }}>
                  {' '}
                  Barang{' '}
                </Text>
                <Image
                  source={require('../assets/images/SepatuNB.png')}
                  style={{
                    width: 100,
                    height: 100,
                    resizeMode: 'contain',
                  }}
                />
              </TouchableOpacity>
              <View style={{marginStart: 10}}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}></View>
                <TouchableOpacity>
                  <View>
                    <Text
                      style={{
                        paddingTop: 25,
                        color: 'black',
                        fontSize: 15,
                        fontWeight: '900',
                      }}>
                      New Balance - Pink Abu - 40
                    </Text>
                  </View>
                  <Text
                    style={{
                      paddingTop: 10,
                      fontSize: 15,
                      fontWeight: '800',
                      color: '#D8D8D8',
                    }}>
                    Cuci sepatu
                  </Text>
                  <Text
                    style={{
                      paddingTop: 10,
                      fontSize: 15,
                      fontWeight: '800',
                      color: '#D8D8D8',
                    }}>
                    Note : -
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
        {/* Pemesanan =========================================================================================== */}
        <View style={{marginEnd: 10, marginStart: 10, marginTop: 240}}>
          <TouchableOpacity
            onPress={() => navigation.navigate('ReservasiSukses')}
            style={{
              width: '100%',
              backgroundColor: '#BB2427',
              borderRadius: 8,
              paddingVertical: 15,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: '#fff',
                fontSize: 16,
                fontWeight: 'bold',
              }}>
              Reservasi Sekarang
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};
export default Summary;
