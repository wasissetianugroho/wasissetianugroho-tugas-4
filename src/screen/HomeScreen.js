import React from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  TextInput,
  TouchableOpacity,
} from 'react-native';

const HomeScreen = ({navigation}) => {
  return (
    <ScrollView>
      <View style={{flex: 1, padding: 20, backgroundColor: 'white'}}>
        {/*Top Content===================================================================================  */}
        <View
          style={{
            Flex: 1,
            flexDirection: 'row',
            paddingTop: -7,
            justifyContent: 'space-between',
          }}>
          <Image
            style={{width: 60, height: 60, borderRadius: 10}}
            source={require('../assets/images/Wasis.jpg')}
          />
          <Image
            style={{width: 35, height: 35}}
            source={require('../assets/icon/Tas.png')}
          />
        </View>
        {/* Nama profil======================================================================================= */}
        <View style={{paddingTop: 12}}>
          <View>
            <Text
              style={{
                fontWeight: '500',
              }}>
              Hello, Wasis!
            </Text>
          </View>
          <View
            style={{
              paddingVertical: 10,
              paddingRight: 20,
            }}>
            <Text
              style={{
                fontSize: 25,
                fontWeight: '900',
                color: 'black',
              }}>
              Ingin merawat dan perbaiki sepatumu? cari disini
            </Text>
          </View>
        </View>
        <View>
          {/* Search================================================================================================= */}
          <View
            style={{
              flex: 1,
              paddingVertical: 10,
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              backgroundColor: 'white',
            }}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                backgroundColor: '#F6F8FF',
                alignItems: 'center',
                borderRadius: 10,
                marginRight: 10,
              }}>
              <Image
                style={{
                  width: 30,
                  height: 30,
                }}
                source={require('../assets/icon/Search.png')}
              />
              <TextInput
                style={{
                  flex: 1,
                  marginLeft: 10,
                }}
              />
            </View>
            <TouchableOpacity>
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  paddingHorizontal: 15,
                  backgroundColor: '#F6F8FF',
                  borderRadius: 10,
                  height: 30,
                  flexDirection: 'row',
                }}>
                <Image
                  style={{
                    width: 20,
                    height: 20,
                    margin: 5,
                  }}
                  source={require('../assets/icon/Filter.png')}
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      {/* CARD================================================================================================ */}
      <View style={{flex: 1, padding: 20}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            flex: 1,
          }}>
          <TouchableOpacity>
            <View
              style={{
                flex: 1,
                padding: 10,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column',
                backgroundColor: 'white',
                borderRadius: 20,
              }}>
              <Image
                source={require('../assets/icon/Sepatu.png')}
                style={{width: 90, height: 90}}
              />
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: '800',
                  color: 'red',
                }}>
                Sepatu
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View
              style={{
                flex: 1,
                padding: 10,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column',
                backgroundColor: 'white',
                borderRadius: 20,
              }}>
              <Image
                source={require('../assets/icon/Jaket.png')}
                style={{width: 90, height: 90}}
              />
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: '800',
                  color: 'red',
                }}>
                Jaket
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View
              style={{
                flex: 1,
                padding: 10,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column',
                backgroundColor: 'white',
                borderRadius: 20,
              }}>
              <Image
                source={require('../assets/icon/Ransel.png')}
                style={{width: 90, height: 90}}
              />
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: '800',
                  color: 'red',
                }}>
                Tas
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
      {/* Rekomendasi========================================================================================= */}
      <View style={{flex: 1, padding: 20}}>
        <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              paddingHorizontal: -10,
              justifyContent: 'flex-start',
              paddingTop: -20,
            }}>
            <Text
              style={{
                fontSize: 15,
                fontWeight: '600',
                color: 'black',
              }}>
              {' '}
              Rekomendasi Terdekat{' '}
            </Text>
          </View>
          <Text
            style={{
              fontSize: 15,
              fontWeight: '600',
              paddingEnd: 15,
              color: 'red',
              paddingTop: -20,
            }}>
            View All
          </Text>
        </View>
      </View>
      {/* Rating 1 =================================================================================== */}
      <View>
        <View
          style={{
            padding: 20,
            backgroundColor: 'white',
            borderRadius: 10,
            marginBottom: 10,
          }}>
          <View style={{flexDirection: 'row', width: '80%'}}>
            <TouchableOpacity>
              <Image
                source={require('../assets/images/Foto1.png')}
                style={{
                  width: 100,
                  height: 150,
                  resizeMode: 'contain',
                }}
              />
            </TouchableOpacity>
            <View style={{marginStart: 10}}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}>
                <View>
                  <Image
                    source={require('../assets/icon/Bintang.png')}
                    style={{
                      width: 50,
                      height: 8,
                      resizeMode: 'contain',
                    }}
                  />
                  <Text
                    style={{
                      color: '#D8D8D8',
                    }}>
                    4.8 Ratings
                  </Text>
                </View>
                <View style={{justifyContent: 'flex-end', marginEnd: 20}}>
                  <Image
                    source={require('../assets/icon/Love1.png')}
                    style={{
                      width: 13,
                      height: 13,
                      resizeMode: 'contain',
                    }}
                  />
                </View>
              </View>
              <TouchableOpacity>
                <View>
                  <Text
                    style={{
                      paddingTop: 10,
                      color: 'black',
                      fontSize: 20,
                      fontWeight: '900',
                    }}>
                    Jack Repair Gejayan
                  </Text>
                </View>
                <View>
                  <Text
                    style={{
                      paddingTop: 10,
                      fontSize: 18,
                      fontWeight: '800',
                      color: '#D8D8D8',
                    }}>
                    Jl. Gejayan III No.2, Karangasem, Kec. Laweyan . . .
                  </Text>
                </View>
                <Text
                  style={{
                    color: 'red',
                    backgroundColor: '#E64C3C33',
                    borderRadius: 24,
                    fontWeight: '700',
                    alignSelf: 'baseline',
                    padding: 8,
                    marginTop: 8,
                  }}>
                  TUTUP
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
      {/* Rating 2========================================================================================== */}
      <View>
        <View
          style={{
            padding: 20,
            backgroundColor: 'white',
            borderRadius: 10,
          }}>
          <View style={{flexDirection: 'row', width: '80%'}}>
            <TouchableOpacity
              onPress={() => navigation.navigate('DetailScreen')}>
              <Image
                source={require('../assets/images/Foto2.png')}
                style={{
                  width: 100,
                  height: 150,
                  resizeMode: 'contain',
                }}
              />
            </TouchableOpacity>
            <View style={{marginStart: 10}}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}>
                <View>
                  <Image
                    source={require('../assets/icon/Bintang.png')}
                    style={{
                      width: 50,
                      height: 8,
                      resizeMode: 'contain',
                    }}
                  />
                  <Text
                    style={{
                      color: '#D8D8D8',
                    }}>
                    4.7 Ratings
                  </Text>
                </View>
                <View style={{justifyContent: 'flex-end', marginEnd: 20}}>
                  <Image
                    source={require('../assets/icon/Love2.png')}
                    style={{
                      width: 15,
                      height: 13,
                      color: '#ADADAD',
                      resizeMode: 'contain',
                    }}
                  />
                </View>
              </View>
              <TouchableOpacity
                onPress={() => navigation.navigate('DetailScreen')}>
                <View>
                  <Text
                    style={{
                      paddingTop: 10,
                      color: 'black',
                      fontSize: 20,
                      fontWeight: '900',
                    }}>
                    Jack Repair Seturan
                  </Text>
                </View>
                <View>
                  <Text
                    style={{
                      paddingTop: 10,
                      fontSize: 18,
                      fontWeight: '800',
                      color: '#D8D8D8',
                    }}>
                    Jl. Seturan Kec. Laweyan . . .
                  </Text>
                </View>
                <Text
                  style={{
                    color: 'green',
                    backgroundColor: '#11A84E1F',
                    borderRadius: 24,
                    fontWeight: '700',
                    alignSelf: 'baseline',
                    padding: 8,
                    marginTop: 8,
                  }}>
                  BUKA
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default HomeScreen;
