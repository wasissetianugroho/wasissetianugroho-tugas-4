import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Routing from '../Routing';
const ReservasiSukses = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <ScrollView>
        {/* Form Pesan ====================================================================================== */}
        <View style={{padding: 25}}>
          <TouchableOpacity onPress={() => navigation.navigate('Summary')}>
            <Image
              source={require('../assets/icon/Close.png')}
              style={{
                width: 35,
                height: 25,
              }}
            />
          </TouchableOpacity>
        </View>
        {/* Reservasi Done ========================================================================================= */}
        <View
          style={{
            padding: 10,
            alignSelf: 'center',
            marginTop: 100,
          }}>
          <Text style={{fontSize: 30, color: '#11A84E'}}>
            Reservasi Berhasil
          </Text>
        </View>
        <View>
          <Image
            source={require('../assets/images/CentangHijau.png')}
            style={{
              width: 220,
              height: 220,
              marginHorizontal: 80,
              marginTop: 30,
            }}
          />
        </View>
        <View>
          <Text style={{fontSize: 25, paddingTop: 40, alignSelf: 'center'}}>
            Kami Telah Mengirimkan Kode
          </Text>
          <Text style={{fontSize: 25, alignSelf: 'center'}}>
            Reservasi Ke Menu Transaksi
          </Text>
        </View>
        {/* Kode Reservasi =========================================================================================== */}
        <View style={{marginEnd: 10, marginStart: 10, marginTop: 170}}>
          <TouchableOpacity
            style={{
              width: '100%',
              backgroundColor: '#BB2427',
              borderRadius: 8,
              paddingVertical: 15,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: '#fff',
                fontSize: 16,
                fontWeight: 'bold',
              }}>
              Lihat Kode Reservasi
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};
export default ReservasiSukses;
