import React from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
const DetailScreen = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <ScrollView style={{flexGrow: 1}}>
        {/* Location Image ===================================================================== */}
        <ImageBackground
          style={{
            width: '100%',
            height: 317,
            resizeMode: 'contain',
          }}
          source={require('../assets/images/JackRepairSeturan.png')}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              paddingHorizontal: 24,
              paddingVertical: 22,
            }}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Image
                style={{
                  width: 28,
                  height: 28,
                  resizeMode: 'contain',
                }}
                source={require('../assets/icon/GoBack.png')}
              />
            </TouchableOpacity>
            <Image
              style={{
                width: 24,
                height: 24,
                resizeMode: 'contain',
              }}
              source={require('../assets/icon/TasPutih.png')}
            />
          </View>
        </ImageBackground>
        {/* Nama Toko ============================================================================ */}
        <View
          style={{
            width: '100%',
            backgroundColor: 'white',
            borderRadius: 19,
            paddingTop: 38,
            marginTop: -41,
          }}>
          <Text
            style={{
              fontSize: 20,
              fontWeight: '700',
              color: 'black',
              marginStart: 20,
            }}>
            {' '}
            Jack Repair Seturan
          </Text>
          <View>
            <Image
              source={require('../assets/icon/Bintang.png')}
              style={{
                width: 50,
                height: 8,
                resizeMode: 'contain',
                marginStart: 20,
              }}
            />
          </View>
          {/* Maps ====================================================================================== */}
          <View style={{flex: 1, padding: 20}}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
              <Image
                source={require('../assets/icon/Gmaps.png')}
                style={{
                  width: 24,
                  height: 24,
                  color: '#BB2427',
                }}
              />
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  paddingHorizontal: -10,
                  justifyContent: 'flex-start',
                  paddingTop: -20,
                }}>
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: '600',
                    color: '#979797',
                  }}>
                  {' '}
                  Jl.Affandi (Gejayan), no.15, Sleman Yogyakarta, 55384{' '}
                </Text>
              </View>
              <View style={{justifyContent: 'flex-end', flexDirection: 'row'}}>
                <TouchableOpacity>
                  <Text
                    style={{
                      fontSize: 12,
                      fontWeight: '900',
                      color: '#3471CD',
                    }}>
                    Lihat Maps
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  color: '#11A84E',
                  backgroundColor: '#11A84E1F',
                  borderRadius: 15,
                  fontSize: 12,
                  fontWeight: '700',
                  alignSelf: 'flex-start',
                  padding: 3,
                  marginTop: 8,
                  paddingHorizontal: 15,
                }}>
                BUKA
              </Text>
              <Text
                style={{
                  fontSize: 13,
                  fontWeight: '900',
                  paddingStart: 20,
                  color: 'black',
                  paddingTop: 10,
                }}>
                09.00 - 21.00
              </Text>
            </View>
          </View>
        </View>
        {/* Deskripsi ============================================================================= */}
        <View style={{paddingHorizontal: 10}}>
          <Text
            style={{
              fontSize: 25,
              fontWeight: '800',
              color: 'black',
              paddingBottom: 10,
              paddingStart: 10,
            }}>
            Deskripsi
          </Text>
          <View>
            <Text
              style={{
                fontSize: 21,
                fontWeight: '600',
                color: '#595959',
                paddingStart: 10,
              }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa
              gravida mattis arcu interdum lectus egestas scelerisque. Blandit
              porttitor diam viverra amet nulla sodales aliquet est. Donec enim
              turpis rhoncus quis integer. Ullamcorper morbi donec tristique
              condimentum ornare imperdiet facilisi pretium molestie.
            </Text>
          </View>
          <View>
            <Text
              style={{
                fontSize: 18,
                fontWeight: '600',
                color: 'black',
                paddingBottom: 5,
                paddingStart: 10,
              }}>
              Range Biaya
            </Text>
            <Text
              style={{
                fontSize: 18,
                fontWeight: '600',
                color: '#8D8D8D',
                paddingBottom: 20,
                paddingStart: 10,
              }}>
              Rp 20.000 - 80.000
            </Text>
          </View>
          {/* Tombol Repair ============================================================================= */}
          <TouchableOpacity
            onPress={() => navigation.navigate('FormPemesanan')}
            style={{
              width: '100%',
              backgroundColor: '#BB2427',
              borderRadius: 8,
              paddingVertical: 15,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: '#fff',
                fontSize: 16,
                fontWeight: 'bold',
              }}>
              Repair Disini
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};
export default DetailScreen;
