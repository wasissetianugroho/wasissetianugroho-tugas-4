import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  FlatList,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Routing from '../Routing';
const CheckOutScreen = ({navigation}) => {
  const [metodePembayaran, setMetodePembayaran] = useState([
    {
      metode: 'Bank Transfer',
      checked: false,
      direction: require('../assets/icon/Bank.png'),
      id: 0,
    },
    {
      metode: 'OVO',
      checked: false,
      direction: require('../assets/icon/Ovo.png'),
      id: 1,
    },
    {
      metode: 'Kartu Kredit',
      checked: false,
      direction: require('../assets/icon/Bank.png'),
      id: 2,
    },
  ]);
  const handleItemPress = item => {
    const updatedData = metodePembayaran.map(x =>
      x.id === item.id ? {...x, checked: !x.checked} : {...x, checked: false},
    );
    setMetodePembayaran(updatedData);
  };

  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <ScrollView>
        {/* Form Pesan ====================================================================================== */}
        <View
          style={{
            flexDirection: 'row',
            padding: 25,
            backgroundColor: 'white',
            bottom: 10,
          }}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image
              source={require('../assets/icon/Back.png')}
              style={{
                width: 35,
                height: 25,
                marginTop: 5,
                marginEnd: 5,
              }}
            />
          </TouchableOpacity>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'flex-start',
              paddingStart: 7,
            }}>
            <Text
              style={{
                fontSize: 28,
                fontWeight: '700',
                color: 'black',
              }}>
              Checkout
            </Text>
          </View>
        </View>
        {/* Data Cust ========================================================================================= */}
        <View style={{padding: 10, backgroundColor: 'white'}}>
          <View>
            <Text
              style={{
                paddingBottom: 5,
                fontSize: 15,
                color: '#979797',
                paddingHorizontal: 10,
              }}>
              Data Customer
            </Text>
            <Text style={{fontSize: 15, paddingHorizontal: 10}}>
              Wasis Setia N ( 081393530013 )
            </Text>
            <Text style={{fontSize: 15, paddingHorizontal: 10}}>
              Jl.perumnas, Condong catur, Sleman, Yogyakarta
            </Text>
            <Text style={{fontSize: 15, paddingHorizontal: 10}}>
              gantengdoang@dipanggang.com{' '}
            </Text>
          </View>
        </View>
        {/* Alamat Tujuan ===================================================================================== */}
        <View
          style={{
            padding: 8,
            backgroundColor: 'white',
            marginTop: 10,
          }}>
          <View>
            <Text
              style={{
                paddingBottom: 5,
                fontSize: 15,
                color: '#979797',
                paddingHorizontal: 10,
              }}>
              Alamat Outlet Tujuan
            </Text>
            <Text style={{fontSize: 15, paddingHorizontal: 10}}>
              Jack Repair - Seturan (027-343457)
            </Text>
            <Text style={{fontSize: 15, paddingHorizontal: 10}}>
              Jl. Affandi No 18, Sleman, Yogyakarta
            </Text>
          </View>
        </View>
        {/* Sepatu Pilihan ==================================================================================== */}
        <View>
          <View
            style={{
              flex: 1,
              padding: 20,
              backgroundColor: 'white',
              marginTop: 8,
            }}>
            <Text
              style={{
                paddingBottom: 5,
                fontSize: 14,
              }}>
              {' '}
              Barang{' '}
            </Text>
            <View style={{flexDirection: 'row', width: '80%'}}>
              <Image
                source={require('../assets/images/SepatuNB.png')}
                style={{
                  width: 100,
                  height: 100,
                  resizeMode: 'contain',
                }}
              />
              <View style={{marginStart: 10}}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}></View>
                <TouchableOpacity>
                  <View>
                    <Text
                      style={{
                        color: 'black',
                        fontSize: 15,
                        fontWeight: '900',
                      }}>
                      New Balance - Pink Abu - 40
                    </Text>
                  </View>
                  <Text
                    style={{
                      paddingTop: 10,
                      fontSize: 15,
                      fontWeight: '800',
                      color: '#D8D8D8',
                    }}>
                    Cuci sepatu
                  </Text>
                  <Text
                    style={{
                      paddingTop: 10,
                      fontSize: 15,
                      fontWeight: '800',
                      color: '#D8D8D8',
                    }}>
                    Note : -
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: '900',
                  color: 'black',
                  paddingTop: 10,
                }}>
                1 pasang
              </Text>
              <Text
                style={{
                  color: 'black',
                  fontSize: 16,
                  fontWeight: 'bold',
                  marginTop: 8,
                  paddingHorizontal: 19,
                }}>
                @Rp 50.000
              </Text>
            </View>
          </View>
        </View>
        {/* Rincian Pembayaran ================================================================================= */}
        <View
          style={{
            padding: 8,
            backgroundColor: 'white',
            marginTop: 10,
          }}>
          <Text
            style={{
              paddingBottom: 5,
              fontSize: 15,
              color: '#979797',
              paddingHorizontal: 10,
            }}>
            Rincian Pembayaran
          </Text>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text
              style={{
                fontSize: 16,
                fontWeight: '400',
                color: 'black',
                paddingTop: 10,
                paddingStart: 10,
              }}>
              Cuci Sepatu {'   '}
            </Text>
            <View
              style={{
                flex: 1,
                justifyContent: 'flex-start',
                paddingTop: 10,
              }}>
              <Text
                style={{
                  fontSize: 15,
                  fontWeight: '600',
                  color: '#FFC107',
                }}>
                {' '}
                x1 Pasang
              </Text>
            </View>
            <Text
              style={{
                color: 'black',
                fontSize: 16,
                fontWeight: '400',
                marginTop: 8,
                paddingHorizontal: 19,
              }}>
              Rp 30.000
            </Text>
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text
              style={{
                fontSize: 16,
                fontWeight: '400',
                color: 'black',
                paddingTop: 10,
                paddingStart: 10,
              }}>
              Biaya Antar
            </Text>
            <Text
              style={{
                color: 'black',
                fontSize: 16,
                fontWeight: '400',
                marginTop: 8,
                paddingHorizontal: 19,
              }}>
              Rp {'  '}3.000
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              borderTopWidth: 1,
              borderTopColor: '#EDEDED',
            }}>
            <Text
              style={{
                fontSize: 16,
                fontWeight: '400',
                color: 'black',
                paddingTop: 10,
                paddingStart: 10,
              }}>
              Total
            </Text>
            <Text
              style={{
                color: 'black',
                fontSize: 16,
                fontWeight: '400',
                marginTop: 8,
                paddingHorizontal: 19,
              }}>
              Rp 33.000
            </Text>
          </View>
        </View>
        {/* Pilih Pembayaran ================================================================================== */}
        <View
          style={{
            paddingHorizontal: 10,
            backgroundColor: 'white',
            marginTop: 12,
            paddingVertical: 18,
          }}>
          <View>
            <Text
              style={{
                fontSize: 15,
                color: '#979797',
                fontWeight: 'bold',
                paddingStart: 10,
              }}>
              Pilih Pembayaran
            </Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 16}}>
            <FlatList
              horizontal={true}
              contentContainerStyle={{flexDirection: 'row'}}
              data={metodePembayaran}
              renderItem={({item}) => (
                <TouchableOpacity
                  onPress={() => handleItemPress(item)}
                  style={{
                    width: 150,
                    marginHorizontal: 10,
                    borderWidth: 1,
                    borderRadius: 4,
                    paddingHorizontal: 22,
                    paddingVertical: 16,
                    borderColor: '#E1E1E1',
                    backgroundColor: item.checked ? '#03426216' : 'white',
                  }}>
                  <Image
                    style={{
                      width: 40,
                      height: 40,
                      resizeMode: 'contain',
                      alignSelf: 'center',
                    }}
                    source={item.direction}
                  />
                  <Text style={{alignSelf: 'center', marginTop: 8}}>
                    {item.metode}
                  </Text>
                </TouchableOpacity>
              )}
            />
          </View>
        </View>
        {/* Tombol Pesan =========================================================================================== */}
        <View style={{padding: 20, backgroundColor: 'white', marginTop: 14}}>
          <TouchableOpacity
            // onPress={() => navigation.navigate('ReservasiScreen')}
            style={{
              backgroundColor: '#BB2427',
              borderRadius: 8,
              padding: 19,
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: '#fff',
                fontSize: 16,
                fontWeight: '700',
              }}>
              Pesan Sekarang
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  radioButton: {
    borderRadius: 1,
    marginVertical: 8,
    marginEnd: 10,
    borderWidth: 1,
    borderColor: 'grey',
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: 10,
  },
});

export default CheckOutScreen;
