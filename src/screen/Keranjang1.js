import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Routing from '../Routing';
const Keranjang1 = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <ScrollView>
        {/* Form Pesan ====================================================================================== */}
        <View
          style={{
            flexDirection: 'row',
            padding: 25,
            backgroundColor: 'white',
            bottom: 10,
          }}>
          <TouchableOpacity onPress={() => navigation.navigate('DetailScreen')}>
            <Image
              source={require('../assets/icon/Back.png')}
              style={{
                width: 35,
                height: 25,
                marginTop: 5,
                marginEnd: 5,
              }}
            />
          </TouchableOpacity>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'flex-start',
              paddingStart: 7,
            }}>
            <Text
              style={{
                fontSize: 28,
                fontWeight: '700',
                color: 'black',
              }}>
              Keranjang
            </Text>
          </View>
        </View>
        {/* Sepatu Pilihan ==================================================================================== */}
        <View
          style={{
            padding: 20,
            backgroundColor: 'white',
            borderRadius: 20,
            marginBottom: 10,
            marginEnd: 10,
            marginStart: 10,
          }}>
          <View style={{flexDirection: 'row', width: '80%'}}>
            <TouchableOpacity>
              <Image
                source={require('../assets/images/SepatuNB.png')}
                style={{
                  width: 100,
                  height: 100,
                  resizeMode: 'contain',
                }}
              />
            </TouchableOpacity>
            <View style={{marginStart: 10}}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}></View>
              <TouchableOpacity>
                <View>
                  <Text
                    style={{
                      paddingTop: 10,
                      color: 'black',
                      fontSize: 15,
                      fontWeight: '900',
                    }}>
                    New Balance - Pink Abu - 40
                  </Text>
                </View>
                <Text
                  style={{
                    paddingTop: 10,
                    fontSize: 15,
                    fontWeight: '800',
                    color: '#D8D8D8',
                  }}>
                  Cuci sepatu
                </Text>
                <Text
                  style={{
                    paddingTop: 10,
                    fontSize: 15,
                    fontWeight: '800',
                    color: '#D8D8D8',
                  }}>
                  Note : -
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        {/* Penambahan Barang ============================================================================================ */}
        <View>
          <TouchableOpacity>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                paddingHorizontal: 30,
                paddingTop: 40,
              }}>
              <Image
                source={require('../assets/icon/Plus.png')}
                style={{
                  width: 30,
                  height: 30,
                  marginStart: 70,
                }}
              />
              <Text
                style={{
                  color: '#BB2427',
                  fontWeight: 'bold',
                  paddingTop: 4,
                  fontSize: 20,
                  paddingStart: 5,
                }}>
                Tambah Barang{' '}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        {/* Masukkan Keranjang =========================================================================================== */}
        <View style={{marginEnd: 10, marginStart: 10, marginTop: 450}}>
          <TouchableOpacity
            onPress={() => navigation.navigate('Summary')}
            style={{
              width: '100%',
              backgroundColor: '#BB2427',
              borderRadius: 8,
              paddingVertical: 15,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: '#fff',
                fontSize: 16,
                fontWeight: 'bold',
              }}>
              Selanjutnya
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};
export default Keranjang1;
