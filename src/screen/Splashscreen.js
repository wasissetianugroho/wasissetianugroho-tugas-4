import React, {useEffect} from 'react';
import {View, Image} from 'react-native';

const Splashscreen = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('AuthNavigation');
    }, 2000);
  }, []);

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
      }}>
      <Image
        source={require('../assets/images/JackFixer.png')}
        style={{
          width: 200,
          height: 200,
        }}
      />
    </View>
  );
};

export default Splashscreen;
