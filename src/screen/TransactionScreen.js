import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Routing from '../Routing';
const TransactionScreen = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <ScrollView>
        {/* Form Pesan ====================================================================================== */}
        <View
          style={{
            flexDirection: 'row',
            padding: 25,
            backgroundColor: 'white',
            bottom: 10,
          }}>
          <TouchableOpacity>
            {/* onPress={() => navigation.navigate('DetailScreen')}> */}
            <Image
              source={require('../assets/icon/Back.png')}
              style={{
                width: 35,
                height: 25,
                marginTop: 5,
                marginEnd: 5,
              }}
            />
          </TouchableOpacity>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'flex-start',
              paddingStart: 7,
            }}>
            <Text
              style={{
                fontSize: 28,
                fontWeight: '700',
                color: 'black',
              }}>
              Transaksi
            </Text>
          </View>
        </View>
        {/* Data Transaksi ==================================================================================== */}
        <TouchableOpacity onPress={() => navigation.navigate('KodeReservasi')}>
          <View
            style={{
              padding: 20,
              backgroundColor: 'white',
              marginEnd: 10,
              marginStart: 10,
              borderRadius: 8,
            }}>
            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  color: '#BDBDBD',
                  fontSize: 15,
                  fontWeight: '700',
                }}>
                20 Desember 2020
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  fontWeight: '900',
                  color: '#BDBDBD',
                  paddingHorizontal: 15,
                }}>
                09.00
              </Text>
            </View>
            <Text style={{fontSize: 15, fontWeight: '900'}}>
              New Balance - Pink Abu - 40
            </Text>
            <Text style={{fontSize: 15}}>Cuci Sepatu</Text>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text
                style={{
                  fontSize: 13,
                  fontWeight: '900',
                  color: 'black',
                  paddingTop: 10,
                }}>
                Kode Reservasi : CS201201
              </Text>
              <Text
                style={{
                  color: '#FFC107',
                  backgroundColor: '#F29C1F16',
                  borderRadius: 15,
                  fontSize: 12,
                  fontWeight: '700',
                  padding: 5,
                  marginTop: 8,
                  paddingHorizontal: 19,
                }}>
                Reserved
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};
export default TransactionScreen;
