import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Routing from '../Routing';
const KodeReservasi = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <ScrollView>
        {/* Form Pesan ====================================================================================== */}
        <View
          style={{
            flexDirection: 'row',
            padding: 25,
            backgroundColor: 'white',
            bottom: 5,
          }}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image
              source={require('../assets/icon/Back.png')}
              style={{
                width: 35,
                height: 25,
              }}
            />
          </TouchableOpacity>
        </View>
        {/* Detail Kode ========================================================================================= */}
        <View style={{backgroundColor: 'white', padding: 10}}>
          <View
            style={{
              padding: 20,
              backgroundColor: 'white',
              marginStart: 10,
            }}>
            <View style={{flexDirection: 'row', alignSelf: 'center'}}>
              <Text
                style={{
                  color: '#BDBDBD',
                  fontSize: 15,
                  fontWeight: '700',
                }}>
                20 Desember 2020
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  fontWeight: '900',
                  color: '#BDBDBD',
                  paddingHorizontal: 15,
                }}>
                09.00
              </Text>
            </View>
          </View>
          <Text
            style={{
              color: '#201F26',
              fontSize: 50,
              paddingTop: 20,
              alignSelf: 'center',
              fontWeight: '900',
            }}>
            CS122001
          </Text>
          <Text style={{fontSize: 20, alignSelf: 'center', color: 'black'}}>
            Kode reservasi
          </Text>
          <View>
            <Text
              style={{
                color: '#6F6F6F',
                fontSize: 25,
                paddingTop: 30,
                alignSelf: 'center',
              }}>
              Sebutkan Kode reservasi saat
            </Text>
            <Text style={{fontSize: 25, alignSelf: 'center'}}>
              tiba di outlet
            </Text>
          </View>
        </View>
        {/* Nama Barang ==================================================================================== */}
        <Text
          style={{
            paddingTop: 10,
            paddingStart: 10,
            fontSize: 15,
            fontWeight: '600',
            color: '#201F26',
          }}>
          {' '}
          Barang{' '}
        </Text>
        <TouchableOpacity onPress={() => navigation.navigate('CheckOutScreen')}>
          <View>
            <View
              style={{
                flex: 1,
                padding: 20,
                backgroundColor: 'white',
                marginTop: 10,
                borderRadius: 8,
                marginEnd: 10,
                marginStart: 10,
              }}>
              <View style={{flexDirection: 'row', width: '80%'}}>
                {/* <TouchableOpacity> */}
                <Image
                  source={require('../assets/images/SepatuNB.png')}
                  style={{
                    width: 100,
                    height: 100,
                    resizeMode: 'contain',
                  }}
                />
                {/* </TouchableOpacity> */}
                <View style={{marginStart: 10}}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                    }}></View>
                  {/* <TouchableOpacity> */}
                  <View>
                    <Text
                      style={{
                        color: 'black',
                        fontSize: 15,
                        fontWeight: '900',
                        paddingHorizontal: 10,
                      }}>
                      New Balance - Pink Abu - 40
                    </Text>
                  </View>
                  <Text
                    style={{
                      paddingTop: 10,
                      fontSize: 15,
                      fontWeight: '800',
                      color: '#D8D8D8',
                      paddingHorizontal: 10,
                    }}>
                    Cuci sepatu
                  </Text>
                  <Text
                    style={{
                      paddingTop: 10,
                      fontSize: 15,
                      fontWeight: '800',
                      color: '#D8D8D8',
                      paddingHorizontal: 10,
                    }}>
                    Note : -
                  </Text>
                  {/* </TouchableOpacity> */}
                </View>
              </View>
            </View>
          </View>
        </TouchableOpacity>
        {/* Status Pesanan ===================================================================================== */}
        <Text
          style={{
            paddingTop: 10,
            paddingStart: 10,
            fontSize: 17,
            fontWeight: '600',
            color: '#201F26',
          }}>
          Status Pesanan
        </Text>
        <View
          style={{
            padding: 8,
            backgroundColor: 'white',
            borderRadius: 8,
            marginEnd: 10,
            marginStart: 10,
            paddingTop: 10,
            marginTop: 10,
          }}>
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Image
              source={require('../assets/icon/TitikMerah.png')}
              style={{
                width: 14,
                height: 14,
                color: '#BB2427',
              }}
            />
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                paddingHorizontal: -10,
                justifyContent: 'flex-start',
                paddingTop: -20,
              }}>
              <Text
                style={{
                  fontSize: 15,
                  fontWeight: '600',
                  color: '#979797',
                  paddingHorizontal: 10,
                }}>
                Telah Reservasi
              </Text>
              <View></View>
            </View>
            <View style={{justifyContent: 'flex-end', flexDirection: 'row'}}>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: '900',
                  color: '#A5A5A5',
                }}>
                09.00
              </Text>
            </View>
          </View>
          <View>
            <Text
              style={{
                paddingHorizontal: 25,
                paddingTop: 6,
                fontSize: 10,
                fontWeight: '800',
                color: '#D8D8D8',
              }}>
              20 Desember 2020
            </Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};
export default KodeReservasi;
