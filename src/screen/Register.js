import React from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';
import Routing from '../Routing';
const Login = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <KeyboardAvoidingView
          behavior="padding"
          enabled
          keyboardVerticalOffset={-500}>
          <Image
            source={require('../assets/icon/RectangleCopy.png')}
            style={{
              width: Dimensions.get('window').width,
              height: 317,
            }}
          />
          {/* Selamat datang ================================================================================== */}
          <View
            style={{
              width: '100%',
              backgroundColor: '#fff',
              borderTopLeftRadius: 19,
              borderTopRightRadius: 19,
              paddingHorizontal: 20,
              paddingTop: 38,
              marginTop: -20,
            }}>
            <View>
              <Text style={{fontSize: 35, fontWeight: 'bold', color: 'black'}}>
                {' '}
                Welcom,{'\n'}Please Register{' '}
              </Text>
            </View>
            {/* Nama dkk ==================================================================================== */}
            <Text style={{color: 'red', fontWeight: 'bold'}}>Nama</Text>
            <TextInput
              placeholder="Enter Your Name"
              style={{
                marginTop: 10,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              keyboardType="email-address"
            />
            <Text style={{color: 'red', fontWeight: 'bold', marginTop: 15}}>
              Email
            </Text>
            <TextInput
              placeholder="Youremail@mail.com"
              secureTextEntry={true}
              style={{
                marginTop: 10,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
            />
            <Text style={{color: 'red', fontWeight: 'bold', marginTop: 15}}>
              Password
            </Text>
            <TextInput
              placeholder="Enter password"
              secureTextEntry={true}
              style={{
                marginTop: 10,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
            />
            <Text style={{color: 'red', fontWeight: 'bold', marginTop: 15}}>
              Confirm Password
            </Text>
            <TextInput
              placeholder="Re-entered password"
              secureTextEntry={true}
              style={{
                marginTop: 10,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
            />
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 15,
                justifyContent: 'space-between',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                {/* Logo ======================================================================================= */}
                <TouchableOpacity>
                  <Image
                    source={require('../assets/icon/Gmail.png')}
                    style={{
                      width: 30,
                      height: 20,
                      resizeMode: 'contain',
                    }}
                  />
                </TouchableOpacity>
                <TouchableOpacity>
                  <Image
                    source={require('../assets/icon/Facebook.png')}
                    style={{
                      width: 20,
                      height: 20,
                      marginHorizontal: 10,
                      resizeMode: 'contain',
                    }}
                  />
                </TouchableOpacity>
                <TouchableOpacity>
                  <Image
                    source={require('../assets/icon/Twitter.png')}
                    style={{
                      width: 20,
                      height: 20,
                      resizeMode: 'contain',
                    }}
                  />
                </TouchableOpacity>
              </View>
            </View>
            {/* Login =========================================================================================== */}
            <TouchableOpacity
              onPress={() => navigation.navigate('AuthNavigation')}
              style={{
                width: '100%',
                marginTop: 30,
                backgroundColor: '#BB2427',
                borderRadius: 8,
                paddingVertical: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 16,
                  fontWeight: 'bold',
                }}>
                Login
              </Text>
            </TouchableOpacity>
            {/* Kata Bawah ================================================================================== */}
            <View
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 20,
                flexDirection: 'row',
              }}>
              <Text
                style={{
                  marginTop: 50,
                  fontSize: 12,
                  color: '#717171',
                }}>
                Don't Have An Account yet?
              </Text>
              <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                <Text
                  style={{
                    marginTop: 50,
                    fontSize: 14,
                    color: '#BB2427',
                    marginLeft: 5,
                  }}>
                  Register
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};
export default Login;
