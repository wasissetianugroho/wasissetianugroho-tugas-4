import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Routing from '../Routing';
import CheckBox from '@react-native-community/checkbox';

const FormPemesanan = ({navigation, route}) => {
  const [data, setData] = useState([
    {key: 'Ganti Sol Sepatu', checked: false},
    {key: 'Jahit Sepatu', checked: false},
    {key: 'Repaint Sepatu', checked: false},
    {key: 'Cuci Sepatu', checked: false},
  ]);
  const handleItemPress = item => {
    const updatedData = data.map(x =>
      x.key === item.key ? {...x, checked: !x.checked} : x,
    );
    setData(updatedData);
  };
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView>
        {/* Form Pesan ====================================================================================== */}
        <View
          style={{
            flexDirection: 'row',
            padding: 25,
            backgroundColor: '#DCDCDC10',
            bottom: 10,
          }}>
          <TouchableOpacity onPress={() => navigation.navigate('DetailScreen')}>
            <Image
              source={require('../assets/icon/Back.png')}
              style={{
                width: 35,
                height: 25,
                marginTop: 5,
                marginEnd: 5,
              }}
            />
          </TouchableOpacity>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'flex-start',
              paddingStart: 7,
            }}>
            <Text
              style={{
                fontSize: 28,
                fontWeight: '700',
                color: 'black',
              }}>
              Formulir Pemesanan
            </Text>
          </View>
        </View>
        {/* Merk,dkk ==================================================================================== */}
        <View
          style={{
            backgroundColor: 'white',
            paddingHorizontal: 20,
            paddingTop: 10,
            marginTop: -20,
          }}>
          <Text style={{color: 'red', fontWeight: 'bold'}}>Merk</Text>
          <TextInput
            placeholder="Masukkan Merk Barang"
            style={{
              marginTop: 10,
              borderRadius: 8,
              backgroundColor: '#F6F8FF',
              paddingHorizontal: 10,
            }}
            keyboardType="default"
          />
          <Text style={{color: 'red', fontWeight: 'bold', marginTop: 15}}>
            Warna
          </Text>
          <TextInput
            placeholder="Warna Barang, cth: Merah - Putih"
            secureTextEntry={true}
            style={{
              marginTop: 10,
              borderRadius: 8,
              backgroundColor: '#F6F8FF',
              paddingHorizontal: 10,
            }}
            keyboardType="default"
          />
          <Text style={{color: 'red', fontWeight: 'bold', marginTop: 15}}>
            Ukuran
          </Text>
          <TextInput
            placeholder="Cth : S,M,L/39,40"
            secureTextEntry={true}
            style={{
              marginTop: 10,
              borderRadius: 8,
              backgroundColor: '#F6F8FF',
              paddingHorizontal: 10,
            }}
            keyboardType="default"
          />
          <Text style={{color: 'red', fontWeight: '600', marginTop: 15}}>
            Photo
          </Text>
          <TouchableOpacity
            style={{
              alignSelf: 'baseline',
              alignItems: 'center',
              borderColor: '#BB2427',
              width: 84,
              borderWidth: 1,
              borderRadius: 8,
              marginTop: 24,
              paddingTop: 24,
              paddingBottom: 12,
              paddingHorizontal: 8,
            }}>
            <Image
              style={{
                width: 25,
                height: 20,
                marginBottom: 12,
              }}
              source={require('../assets/icon/Camera.png')}
            />
            <Text> Add Photo</Text>
          </TouchableOpacity>
          {/* Centang ================================================================================== */}
          <FlatList
            data={data}
            renderItem={({item}) => (
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginVertical: 4,
                }}>
                <CheckBox
                  value={item.checked}
                  onValueChange={() => handleItemPress(item)}
                />
                <Text>
                  {item.key} {item.checked} {item.id}
                </Text>
              </View>
            )}
          />
          {/* Catatan ================================================================================== */}
          <View>
            <Text style={{color: 'red', fontWeight: 'bold', marginTop: 20}}>
              Catatan
            </Text>
            <TextInput
              placeholder="Cth : ingin ganti sol baru"
              secureTextEntry={true}
              style={{
                marginTop: 8,
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
                marginBottom: 50,
              }}
              keyboardType="default"
            />
          </View>
          {/* Masukkan Keranjang =========================================================================================== */}
          <TouchableOpacity
            onPress={() => navigation.navigate('Keranjang1')}
            style={{
              width: '100%',
              backgroundColor: '#BB2427',
              borderRadius: 8,
              paddingVertical: 15,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: '#fff',
                fontSize: 16,
                fontWeight: 'bold',
              }}>
              Masukkan Keranjang
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};
export default FormPemesanan;
